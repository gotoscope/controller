// system.c
//
// Links the subsystems and runs the firmware.
#include "system.h"

// Initialises the system.
ErrorCode system_init(SystemState* system) {
    ErrorCode err;
    // Initialise telescope mount
    if ((err = telescope_mount_init(&system->mount))) {
        return err;
    }
    // Initialise serial reader
    if ((err = uart_init())) {
        return err;
    }

    return ERR_OK;
}

// Runs a single update step of the system.
ErrorCode system_update(SystemState* system) {
    ErrorCode err;
    // Process any data in SerialReader.
    while (uart_data_available()) {
        char buffer[INPUT_BUFFER_SIZE];
        if ((err = uart_read(&buffer[0]))) {
            return err;
        }

        if ((err = telescope_controller_handle(&system->mount, buffer))) {
            return err;
        }
    }

    // Update telescope mount
    telescope_mount_update(&system->mount);

    // Delay and then return
    _delay_ms(SYS_UPDATE_PERIOD);
    return ERR_OK;
}
