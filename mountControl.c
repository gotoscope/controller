// mountControl.c
//
// Logic for controlling the telescope mount
#include "mountControl.h"

// Initialises the telescope mount.
ErrorCode telescope_mount_init(TelescopeMount* mount) {
    // Initialise motors
    const unsigned char altitudePins[] = ALTITUDE_MOTOR_PINS;
    const unsigned char azimuthPins[] = AZIMUTH_MOTOR_PINS;
    stepper_init(&mount->altitudeMotor, ALTITUDE_MOTOR_PORT, altitudePins);
    stepper_init(&mount->azimuthMotor, AZIMUTH_MOTOR_PORT, azimuthPins);
    
    return ERR_OK;
}

// Sets the target to point towards.
void telescope_mount_target(TelescopeMount* mount, float targetAlt,
        float targetAzi) {
    // Altitude stepper
    MotorDirection dir = DIR_ANTI_CLOCKWISE;
    if (targetAlt - telescope_mount_get_altitude(mount) < 0) {
        dir = DIR_CLOCKWISE;
    }
    stepper_target(&mount->altitudeMotor, targetAlt, dir);

    // Azimuth stepper
    dir = DIR_ANTI_CLOCKWISE;
    if (targetAzi - telescope_mount_get_azimuth(mount) < 0) {
        dir = DIR_CLOCKWISE;
    }
    stepper_target(&mount->azimuthMotor, targetAzi, dir);
}

// Performs an update of the telescope mount.
ErrorCode telescope_mount_update(TelescopeMount* mount) {
    stepper_update(&mount->altitudeMotor);
    stepper_update(&mount->azimuthMotor);
    return ERR_OK;
}

// Gets the current position (as an angle) of the altitude stepper.
float telescope_mount_get_altitude(TelescopeMount* mount) {
    return DEGREES_PER_STEP * mount->altitudeMotor.position;
}

// Gets the current position (as an angle) of the azimuth stepper.
float telescope_mount_get_azimuth(TelescopeMount* mount) {
    return DEGREES_PER_STEP * mount->azimuthMotor.position;
}

// Handles a message string and updates the telescope mount.
ErrorCode telescope_controller_handle(TelescopeMount* mount,
        const char msg[]) {
    char opcode[BUFFER_SIZE], dummy;
    int targetAlt, targetAzi;
    
    // Try read message
    int count = sscanf(msg, "%[a-zA-Z] %d %d %c", opcode, &targetAlt,
            &targetAzi, &dummy);
    if (count <= 0) {
        return ERR_INVALID_MESSAGE;
    }

    // Handle message
    if (strcmp(opcode, OPCODE_SET_TARGET) == 0) {
        if (count != 3) {
            return ERR_INVALID_MESSAGE;
        }

        telescope_mount_target(mount, targetAlt / PRECISION,
                targetAzi / PRECISION);
    } else {
        return ERR_INVALID_MESSAGE;
    }

    return ERR_OK;
}
