// serialReader.c
//
// Read data from UART into a buffer for processing.
#include "serialReader.h"

// Global UartBuffer
static UartBuffer uartBuffer;

// Initialises the UART interface
ErrorCode uart_init() {
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;
#if USE_2X
    UCSR0A |= (1 << U2X0);
#else
    UCSR0A &= ~(1 << U2X0);
#endif
    
    UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); // Use 8bit data
    UCSR0B = (1 << RXEN0) | (1 << TXEN0); // Enable RX & TX
    UCSR0B |= (1 << RXCIE0); // Enable received interrupt

    uartBuffer.size = 0;
    uartBuffer.ready = false;

    sei();
    return ERR_OK;
}

// Returns whether there is a packet available to read
bool uart_data_available() {
    return uartBuffer.ready;
}

// Reads the available data into a char array
ErrorCode uart_read(char* buffer) {
    if (!uartBuffer.ready) {
        return ERR_NO_DATA;
    }
    
    for (int i = 0; i < uartBuffer.size; i++) {
        buffer[i] = uartBuffer.buffer[i];
    }
    uartBuffer.ready = false;
    uartBuffer.size = 0;

    return ERR_OK;
}

// Interrupt vector when data is available to read.
ISR(USART_RX_vect) {
    char data = UDR0;
    uartBuffer.buffer[uartBuffer.size++] = data;
    if (data == '\r') {
        uartBuffer.ready = true;
    }
}
