// mountControl.h
//
// Logic for the telescope mount
#ifndef MOUNT_CONTROL_H
#define MOUNT_CONTROL_H

// C Libraries
#include <stdio.h>
#include <string.h>

// AVR Libraries

// Project Libraries
#include "error.h"
#include "stepper.h"

// CONSTANTS =================================================================
// Stepper motor pins
#define ALTITUDE_MOTOR_PORT 0x08 // PORTC
#define AZIMUTH_MOTOR_PORT 0x05 // PORTB
#define ALTITUDE_MOTOR_PINS {0,1,2,3}
#define AZIMUTH_MOTOR_PINS {1,2,3,4}

#define DEGREES_PER_STEP (360.f/SM_TOTAL_STEPS)
#define PRECISION 100.f

#define OPCODE_SET_TARGET "TARGET"

#define BUFFER_SIZE 8

// DATA STRUCTURES ===========================================================
// Stores the state of the telescope mount.
typedef struct TelescopeMount {
    StepperMotor altitudeMotor;
    StepperMotor azimuthMotor;
} TelescopeMount;

// FUNCTION PROTOTYPES =======================================================
// Initialises the telescope mount
ErrorCode telescope_mount_init(TelescopeMount* mount);

// Sets the target to point towards
void telescope_mount_target(TelescopeMount* mount, float targetAlt,
        float targetAzi);

// Performs an update of the telescope mount
ErrorCode telescope_mount_update(TelescopeMount* mount);

// Returns the current position (as an angle) of the altitude stepper.
float telescope_mount_get_altitude(TelescopeMount* mount);

// Returns the current position (as an angle) of the azimuth stepper.
float telescope_mount_get_azimuth(TelescopeMount* mount);

// Handles a message string and updates the telescope mount
ErrorCode telescope_controller_handle(TelescopeMount* mount,
        const char msg[]);

#endif // End header guard
