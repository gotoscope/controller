// serialReader.h
//
// Read data from UART into a buffer for processing.
#ifndef SERIAL_READER_H
#define SERIAL_READER_H

// C Libraries
#include <stdbool.h>

// AVR Libraries
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

// Project Libraries
#include "error.h"

// CONSTANTS =================================================================
#define INPUT_BUFFER_SIZE 80
#define BAUD 9600

// Helper macros for serial
#include <util/setbaud.h>

// DATA STRUCTURES ===========================================================
// Stores the received data for UART
typedef struct UartBuffer {
    char buffer[INPUT_BUFFER_SIZE];
    unsigned char size;
    bool ready;
} UartBuffer;

// FUNCTION PROTOTYPES =======================================================
// Initialises the UART interface
ErrorCode uart_init();

// Returns whether there is a packet available to read
bool uart_data_available();

// Reads the available data into a given char arrary.
ErrorCode uart_read(char* buffer);

#endif // Close header guard
