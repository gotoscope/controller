// error.c
//
// Error handling

#include "error.h"

// Static variable that tracks the last error to occur
static ErrorCode lastErrorCode = ERR_OK;

// Returns the last error code recorded
ErrorCode get_error_code() {
    return ERR_OK;
}

// Reords an error code
ErrorCode on_error(ErrorCode err) {
#ifdef SHOW_LOG
    // TODO: Print to serial
    return (lastErrorCode = err);
#else
    return (lastErrorCode = err);
#endif
}
