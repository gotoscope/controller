// stepper.h
//
// Stepper motor controller

#ifndef STEPPER_H
#define STEPPER_H

// Std Libraries

// AVR Libraries
#include <avr/io.h>

// Project Libraries
#include "portutls.h"

// CONSTANTS =================================================================
#define SM_TOTAL_STEPS 20480 // Total steps for 1 revolution
#define SM_CYCLE_SIZE 8 // Steps per 1 full cycle of motor
#define SM_PIN_COUNT 4 // Number of pins to control motor
#define STEPS_PER_DEGREE (20480/360.f)

// DATA TYPES ================================================================
// Represents the motor direction
typedef enum MotorDirection {
    DIR_CLOCKWISE,
    DIR_ANTI_CLOCKWISE
} MotorDirection;

// Stores the state of a given stepper motor
typedef struct StepperMotor {
    unsigned char pinPort;
    unsigned char controlPins[SM_PIN_COUNT];
    unsigned short position;
    unsigned short target;
    unsigned char posInCycle;
    MotorDirection direction;
} StepperMotor;


// FUNCTION PROTOTYPES =======================================================
// Initialises a stepper motor to a zero state.
void stepper_init(StepperMotor* motor, unsigned char port,
        const unsigned char pins[SM_PIN_COUNT]);

// Calibrates the stepper motor to zero at the current position.
void stepper_calibrate(StepperMotor* motor);

// Sets the target position of the stepper motor in steps and to move in
// the provided direction.
void stepper_target_steps(StepperMotor* motor, unsigned short steps,
        MotorDirection dir);

// Sets the target position of the stepper motor as an angle in degrees, in
// the provided direction.
void stepper_target(StepperMotor* motor, float angle, MotorDirection dir);

// Run a single cycle of the stepper motor. Note: maximum update rate is 100hz
void stepper_update(StepperMotor* motor);

// Sets the output pins of a stepper motor to reflect the current state.
void stepper_output(StepperMotor* motor);

#endif
