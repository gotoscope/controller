// error.h
//
// Error handling for embedded software
#ifndef ERROR_H
#define ERROR_H

// If defined then on_error will attempt to print to serial
#define SHOW_LOG

// Error codes
typedef enum ErrorCode {
    ERR_OK,
    ERR_INVALID_MESSAGE,
    ERR_NO_DATA
} ErrorCode;

// Method that returns the last error code returned
ErrorCode get_error_code();

// Records an error code occurence
ErrorCode on_error(ErrorCode err);

#endif // End header guard
