#Compilers and flags
CC = avr-gcc
CFLAGS = -Os -Wall -pedantic -std=gnu99 -mmcu=atmega328p -DF_CPU=8000000
OBJCOPY = avr-objcopy

# Dependencies and objs
DEPS = error.h mountControl.h portutls.h stepper.h system.h
SRC := $(shell ls *.c | grep -v tests.c | grep -v program.c)
OBJS := $(SRC:.c=.o)

# Output filenames
OUT = out
TEST = tests

# Default target
all: $(OUT).hex $(TEST).hex runtests

# Build only target
build: $(OUT).hex

# Run tests
runtests: $(TEST).hex
	simavr -m atmega328p -f 9600 $(TEST).hex

# Main output
$(OUT).hex: $(OUT).elf
	$(OBJCOPY) -O ihex -R .eeprom $(OUT).elf $(OUT).hex

# Main Elf
$(OUT).elf: program.o $(OBJS)
	$(CC) $(CFLAGS) -o $(OUT).elf program.o $(OBJS)

# Program
program.o: program.c $(DEPS)
	$(CC) $(CFLAGS) -c -o program.o program.c

# Objects
$(OBJS): $(SRC) $(DEPS)
	$(CC) $(CFLAGS) -c -o $@ $(@:.o=.c)

# Test output
$(TEST).hex: $(TEST).elf
	$(OBJCOPY) -O ihex -R .eeprom $(TEST).elf $(TEST).hex

# Test elf
$(TEST).elf: tests.o $(OBJS)
	$(CC) $(CFLAGS) -o $(TEST).elf tests.o $(OBJS)

# Test
tests.o: tests.c $(DEPS)
	$(CC) $(CFLAGS) -c -o tests.o tests.c

clean:
	rm -f -r *.o
	rm -f -r *.hex
	rm -f -r *.elf
