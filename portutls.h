// portutls.h
//
// Support macros for port manipulation
#ifndef PORTUTLS_H
#define PORTUTLS_H

#include <avr/io.h>

// Gets the address of a given port
#define PORT(x) _SFR_IO8(x)

// Gets the direction of a given port
#define DDR(x) (*(&_SFR_IO8(x) - 1))

// Gets the input address of a given port
#define PIN(x) (*(&_SFR_IO8(x) - 2))

#endif // Close header guard
