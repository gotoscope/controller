// program.c
//
// Main entry point for embedded software.
#include "error.h"
#include "system.h"

// Main entry point
int main(void) {
    ErrorCode err;
    SystemState state;

    if ((err = system_init(&state))) {
        // Error occur in start up
        return err;
    }

    while (1) {
        if ((err = system_update(&state))) {
            // Error occurs during running of mount
            return err;
        }
    }
}
