// system.h
//
// Links the subsystems and runs the firmware.
#ifndef SYSTEM_H
#define SYSTEM_H
// C Libraries
#include <stdbool.h>

// AVR Libraries
#include <util/delay.h>

// Project Libraries
#include "error.h"
#include "mountControl.h"
#include "serialReader.h"

// CONSTANTS =================================================================
#define SYS_UPDATE_PERIOD 10 // milliseconds

// DATA STRUCTURES ===========================================================
// Stores the system state
typedef struct SystemState {
    TelescopeMount mount;
} SystemState;

// FUNCTION PROTOTYPES =======================================================
// Initialises the system.
ErrorCode system_init(SystemState* system);

// Runs a single update step of the system.
ErrorCode system_update(SystemState* system);

#endif // Close header guard
