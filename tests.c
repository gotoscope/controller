// tests.c
//
// Runs all unit tests and outputs to serial
// C Libraries
#include <stdio.h>
#include <stdbool.h>

// AVR Libraries
#include <avr/io.h>
#include <avr/sleep.h>

// Project Libraries
#include "error.h"
#include "portutls.h"
#include "mountControl.h"
#include "stepper.h"

// CONSTANTS =================================================================
#define EPSILON 0.000001f

// UART ======================================================================
// Put char for printf
int usart_putchar(char var, FILE* stream) {
    loop_until_bit_is_set(UCSR0A, UDRE0);
    UDR0 = var;
    return 0;
}

// Setup stdout stream
static FILE mystdout = FDEV_SETUP_STREAM(usart_putchar, NULL,
        _FDEV_SETUP_WRITE);

// ASSERTS ===================================================================
// Tests equality of an unsigned char value.
bool assert_equal_char(char* msg, unsigned char expected,
        unsigned char actual) {
    if (expected == actual) {
        return true;
    }
    printf("%s\nExpected: %02X, Actual: %02X\n", msg, expected, actual);
    return false;
}

// Tests equality of an integer value.
bool assert_equal_int(char* msg, int expected, int actual) {
    if (expected == actual) {
        return true;
    }
    printf("%s\nExpected: %d, Actual: %d\n", msg, expected, actual);
    return false;
}

// Tests equality of a floating point value.
bool assert_equal_float(char* msg, float expected, float actual) {
    if (expected - actual < EPSILON && actual - expected < EPSILON) {
        return true;
    }
    printf("%s\nExpected: %f, Actual: %f\n", msg, expected, actual);
    return false;
}

// TESTS: stepper.c ==========================================================
// Tests the stepper_init method. Relies on stepper_output.
bool test_stepper_init() {
    // Set up environment
    PORTB = 0x00;
    DDRB = 0x00;

    // Set up motor and init test
    StepperMotor motor;
    unsigned char pins[4] = {4, 5, 6, 7};
    stepper_init(&motor, 0x05, pins);

    // Check values
    if (!assert_equal_int("Position mismatch", 0, motor.position)) {
        return false;
    }
    if (!assert_equal_int("PosInCycle mismatch", 0, motor.posInCycle)) {
        return false;
    }
    if (!assert_equal_int("Target mismatch", 0, motor.target)) {
        return false;
    }
    if (!assert_equal_char("Direction mismatch", DIR_CLOCKWISE,
                motor.direction)) {
        return false;
    }

    // Check ports
    if (!assert_equal_char("DDRB mismatch", 0xF0, DDRB)) {
        return false;
    }
    if (!assert_equal_char("PORTB mismatch", 0x10, PORTB)) {
        return false;
    }
    
    return true;
}

// Tests the stepper_calibrate method.
bool test_stepper_calibrate() {
    // Setup motor
    StepperMotor motor;
    unsigned char pins[4] = {0, 1, 2, 3};
    stepper_init(&motor, 0x05, pins);

    // Set position and target to any value
    motor.position = 32;
    motor.target = 32;

    // Run tests
    stepper_calibrate(&motor);
    if (!assert_equal_int("Position mismatch", 0, motor.position) ||
            !assert_equal_int("Target mismatch", 0, motor.target)) {
        return false;
    }

    return true;
}

// Tests the stepper_target_steps method.
bool test_stepper_target_steps() {
    // Setup motor
    StepperMotor motor;
    unsigned char pins[4] = {0, 1, 2, 3};
    stepper_init(&motor, 0x05, pins);

    // Run tests
    stepper_target_steps(&motor, 32, DIR_ANTI_CLOCKWISE);
    if (!assert_equal_int("Target mismatch", 32, motor.target) ||
            !assert_equal_char("Direction mismatch", DIR_ANTI_CLOCKWISE,
            motor.direction)) {
        return false;
    }
    return true;
}

// Tests the stepper_target method.
bool test_stepper_target() {
    // Setup motor
    StepperMotor motor;
    unsigned char pins[4] = {0, 1, 2, 3};
    stepper_init(&motor, 0x05, pins);

    // Run tests
    stepper_target(&motor, 90, DIR_ANTI_CLOCKWISE);
    if (!assert_equal_int("Target mismatch", 5120, motor.target) ||
            !assert_equal_char("Direction mismatch", DIR_ANTI_CLOCKWISE,
            motor.direction)) {
        return false;
    }
    return true;
}

// Tests the stepper_update method in the anti clockwise direction.
bool test_stepper_update_aclockwise() {
    // Setup motor
    StepperMotor motor;
    unsigned char pins[4] = {4, 5, 6, 7};
    stepper_init(&motor, 0x05, pins);

    // Set target
    stepper_target_steps(&motor, 2, DIR_ANTI_CLOCKWISE);

    // Run first step tests
    stepper_update(&motor);
    if (!assert_equal_int("Position mismatch", 1, motor.position)
            || !assert_equal_char("PosInCycle mismatch", 7,
            motor.posInCycle)) {
        return false;
    }

    // Run second step tests
    stepper_update(&motor);
    if (!assert_equal_int("Position mismatch", 2, motor.position)
            || !assert_equal_char("PosInCycle mismatch", 6,
            motor.posInCycle)) {
        return false;
    }

    // Run third step tests (should not change)
    stepper_update(&motor);
    if (!assert_equal_int("Position mismatch", 2, motor.position)
            || !assert_equal_char("PosInCycle mismatch", 6,
            motor.posInCycle)) {
        return false;
    }
    return true;
}

// Tests the stepper_update method in the clockwise direction
bool test_stepper_update_clockwise() {
    // Setup motor
    StepperMotor motor;
    unsigned char pins[4] = {4, 5, 6, 7};
    stepper_init(&motor, 0x05, pins);
    motor.position = 2;
    motor.target = 2;

    // Set target
    stepper_target_steps(&motor, 0, DIR_CLOCKWISE);

    // Run first step tests
    stepper_update(&motor);
    if (!assert_equal_int("Position mismatch", 1, motor.position)
            || !assert_equal_char("PosInCycle mismatch", 1,
            motor.posInCycle)) {
        return false;
    }

    // Run second step tests
    stepper_update(&motor);
    if (!assert_equal_int("Position mismatch", 0, motor.position)
            || !assert_equal_char("PosInCycle mismatch", 2,
            motor.posInCycle)) {
        return false;
    }

    // Run third step tests (should not change)
    stepper_update(&motor);
    if (!assert_equal_int("Position mismatch", 0, motor.position)
            || !assert_equal_char("PosInCycle mismatch", 2,
            motor.posInCycle)) {
        return false;
    }
    return true;
}

// Tests the stepper_output function.
bool test_stepper_output() {
    // Setup motor
    StepperMotor motor;
    unsigned char pins[4] = {4, 5, 6, 7};
    stepper_init(&motor, 0x05, pins);

    // Run tests on all positions of output
    motor.posInCycle = 0;
    stepper_output(&motor);
    if (!assert_equal_char("PORTB mismatch", 0x10, PORTB & 0xF0)) {
        return false;
    }

    motor.posInCycle = 1;
    stepper_output(&motor);
    if (!assert_equal_char("PORTB mismatch", 0x30, PORTB & 0xF0)) {
        return false;
    }

    motor.posInCycle = 2;
    stepper_output(&motor);
    if (!assert_equal_char("PORTB mismatch", 0x20, PORTB & 0xF0)) {
        return false;
    }

    motor.posInCycle = 3;
    stepper_output(&motor);
    if (!assert_equal_char("PORTB mismatch", 0x60, PORTB & 0xF0)) {
        return false;
    }

    motor.posInCycle = 4;
    stepper_output(&motor);
    if (!assert_equal_char("PORTB mismatch", 0x40, PORTB & 0xF0)) {
        return false;
    }

    motor.posInCycle = 5;
    stepper_output(&motor);
    if (!assert_equal_char("PORTB mismatch", 0xC0, PORTB & 0xF0)) {
        return false;
    }

    motor.posInCycle = 6;
    stepper_output(&motor);
    if (!assert_equal_char("PORTB mismatch", 0x80, PORTB & 0xF0)) {
        return false;
    }

    motor.posInCycle = 7;
    stepper_output(&motor);
    if (!assert_equal_char("PORTB mismatch", 0x90, PORTB & 0xF0)) {
        return false;
    }

    return true;
}

// TESTS: mountControl.c =====================================================
// Tests telescope_mount_init
bool test_telescope_mount_init() {
    // Init telescope mount
    TelescopeMount mount;
    telescope_mount_init(&mount);

    // Test that the pins and port of the altitude stepper motor are correct
    StepperMotor* altitude = &mount.altitudeMotor;
    if (!assert_equal_char("Altitude port mismatch", 0x08,
            altitude->pinPort)) {
        return false;
    }
    for (int i = 0; i < 4; i++) {
        if (!assert_equal_char("Altitude pin mismatch", i,
                altitude->controlPins[i])) {
            return false;
        }
    }

    // Test that the pins and port of the azimuth stepper motor are correct
    StepperMotor* azimuth = &mount.azimuthMotor;
    if (!assert_equal_char("Azimuth port mismatch", 0x05,
            azimuth->pinPort)) {
        return false;
    }
    for (int i = 0; i < 4; i++) {
        if (!assert_equal_char("Azimuth pin mismatch", i + 1,
                azimuth->controlPins[i])) {
            return false;
        }
    }

    // Test complete
    return true;
}

// Tests the telescope_mount_target function.
bool test_telescope_mount_target() {
    // Set up test structure
    TelescopeMount mount;
    telescope_mount_init(&mount);

    // Run test
    telescope_mount_target(&mount, 90.f, 90.f);

    // Test that the target of the stepper motors has correctly updated.
    if (!assert_equal_int("Altitude target mismatch", 5120,
            mount.altitudeMotor.target) || !assert_equal_char(
            "Altitude direction mismatch", DIR_ANTI_CLOCKWISE,
            mount.altitudeMotor.direction)) {
        return false;
    }
    if (!assert_equal_int("Azimuth target mismatch", 5120,
            mount.azimuthMotor.target) || !assert_equal_char(
            "Azimuth direction mismatch", DIR_ANTI_CLOCKWISE,
            mount.azimuthMotor.direction)) {
        return false;
    }

    return true;
}

// Tests the telescope_mount_update function updates both motors.
bool test_telescope_mount_update() {
    // Set up test structure
    TelescopeMount mount;
    telescope_mount_init(&mount);

    // Set targets for motors
    stepper_target_steps(&mount.altitudeMotor, 1, DIR_ANTI_CLOCKWISE);
    stepper_target_steps(&mount.azimuthMotor, 1, DIR_ANTI_CLOCKWISE);

    // Run test
    telescope_mount_update(&mount);
    if (!assert_equal_int("Altitude position mismatch", 1,
            mount.altitudeMotor.position)) {
        return false;
    }
    if (!assert_equal_int("Azimuth position mismatch", 1,
            mount.azimuthMotor.position)) {
        return false;
    }

    return true;
}

// Tests the getter for the altitude stepper motor
bool test_telescope_mount_get_altitude() {
    // Set up test structure
    TelescopeMount mount;
    telescope_mount_init(&mount);

    // Test zero position
    if (!assert_equal_float("Position mismatch", 0.f,
            telescope_mount_get_altitude(&mount))) {
        return false;
    }

    // Test position at 90d
    mount.altitudeMotor.position = 5120;
    if (!assert_equal_float("Position mismatch", 90.f,
            telescope_mount_get_altitude(&mount))) {
        return false;
    }

    return true;
}

// Tests the getter for the azimuth stepper motor
bool test_telescope_mount_get_azimuth() {
    TelescopeMount mount;
    telescope_mount_init(&mount);

    // Zero position
    if (!assert_equal_float("Position mismatch", 0.f,
            telescope_mount_get_azimuth(&mount))) {
        return false;
    }

    // Test position at 90d
    mount.azimuthMotor.position = 5120;
    if (!assert_equal_float("Position mismatch", 90.f,
            telescope_mount_get_azimuth(&mount))) {
        return false;
    }
    
    return true;
}

// Tests that passing an invalid string to the telescope control returns
// an appropriate errorcode.
bool test_telescope_controller_handle_invalid() {
    TelescopeMount mount;
    telescope_mount_init(&mount);

    // Invalid opcode
    if (!assert_equal_char("ErrorCode mismatch (Opcode)", ERR_INVALID_MESSAGE,
            telescope_controller_handle(&mount, "INAVLID 9 2"))) {
        return false;
    }

    // Empty string
    if (!assert_equal_char("ErrorCode mismatch (Empty)", ERR_INVALID_MESSAGE,
            telescope_controller_handle(&mount, ""))) {
        return false;
    }

    // No parameters
    if (!assert_equal_char("ErrorCode mismatch (Args)", ERR_INVALID_MESSAGE,
            telescope_controller_handle(&mount, "TARGET"))) {
        return false;
    }

    // Invalid parameters
    if (!assert_equal_char("ErrorCode mismatch (Args2)", ERR_INVALID_MESSAGE,
            telescope_controller_handle(&mount, "TARGET A B"))) {
        return false;
    }

    // Too many parameters
    if (!assert_equal_char("ErrorCode mismatch (Args3)", ERR_INVALID_MESSAGE,
            telescope_controller_handle(&mount, "TARGET 23000 123111 123"))) {
        return false;
    }

    return true;
}

// Tests that the telescope controller correctly sets motors based on passed
// parameters.
bool test_telescope_controller_handle_valid() {
    TelescopeMount mount;
    telescope_mount_init(&mount);

    if (!assert_equal_char("ErrorCode mismatch", ERR_OK,
            telescope_controller_handle(&mount, "TARGET 9000 9000"))) {
        return false;
    }

    if (!assert_equal_int("Altitude target mismatch", 5120,
            mount.altitudeMotor.target)) {
        return false;
    }
    if (!assert_equal_int("Azimuth target mismatch", 5120,
            mount.azimuthMotor.target)) {
        return false;
    }

    return true;
}

// RUNNER ====================================================================
// Run all tests
int run_tests(void) {
    struct TestPair {
        bool (*test)(void);
        char* name;
    };

    // Define tests here
    int total = 14;
    const struct TestPair tests[] = {
        {test_stepper_init, "test_stepper_init"},
        {test_stepper_calibrate, "test_stepper_calibrate"},
        {test_stepper_target_steps, "test_stepper_target_steps"},
        {test_stepper_target, "test_stepper_target"},
        {test_stepper_update_clockwise, "test_stepper_update_clockwise"},
        {test_stepper_update_aclockwise, "test_stepper_update_aclockwise"},
        {test_stepper_output, "test_stepper_output"},
        {test_telescope_mount_init, "test_telescope_mount_init"},
        {test_telescope_mount_target, "test_telescope_mount_target"},
        {test_telescope_mount_update, "test_telescope_mount_update"},
        {test_telescope_mount_get_altitude,
                "test_telescope_mount_get_altitude"},
        {test_telescope_mount_get_azimuth,
                "test_telescope_mount_get_azimuth"},
        {test_telescope_controller_handle_invalid,
                "test_telescope_controller_handle_invalid"},
        {test_telescope_controller_handle_valid,
                "test_telescope_controller_handle_valid"}
    };

    // Run all tests
    int pass = 0;
    for (int i = 0; i < total; i++) {
        printf("%s: ", tests[i].name);
        if (tests[i].test()) {
            printf("passed\n");
            pass++;
        } else {
            printf("%s failed\n", tests[i].name);
        }
    }

    // Print reseults
    if (pass == total) {
        printf("\nSuccess\n%d / %d tests passed\n", pass, total);
        return true;
    }

    printf("\nFAILED\n%d / %d tests failed\n", total - pass, total);
    return false;
}

// Test entry point
int main(void) {
    // Init stream
    stdout = &mystdout;

    // Run tests
    printf("Running tests\n\n");
    run_tests();
    sleep_cpu();
    return 0;
}

