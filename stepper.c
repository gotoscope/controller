// stepper.c
//
// Stepper motor controller
#include "stepper.h"

// Initialises a stepper motor to a zero state.
void stepper_init(StepperMotor* motor, unsigned char port,
        const unsigned char pins[SM_PIN_COUNT]) {
    motor->position = 0;
    motor->posInCycle = 0;
    motor->direction = DIR_CLOCKWISE;
    motor->target = 0;

    motor->pinPort = port;
    for (int i = 0; i < SM_PIN_COUNT; i++) {
        motor->controlPins[i] = pins[i];
        
        // Set port dir to out, and set port value to off.
        DDR(motor->pinPort) |= (1 << motor->controlPins[i]);
        PORT(motor->pinPort) &= ~(1 << motor->controlPins[i]);
    }

    stepper_output(motor);
}

// Calibrates the stepper motor to zero at the current position.
void stepper_calibrate(StepperMotor* motor) {
    motor->position = 0;
    motor->target = 0;
}

// Sets the target position of the stepper motor in steps and to move in
// the provided direction.
void stepper_target_steps(StepperMotor* motor, unsigned short steps,
        MotorDirection dir) {
    motor->target = steps;
    motor->direction = dir;
}

// Sets the target position of the stpper motor as an angle in degrees, in
// the provided direction.
void stepper_target(StepperMotor* motor, float angle, MotorDirection dir) {
    stepper_target_steps(motor, (int)(angle * STEPS_PER_DEGREE), dir);
}

// Run a single cycle of the stepper motor. Note: maximum update rate is 100hz
void stepper_update(StepperMotor* motor) {
    if (motor->position == motor->target) {
        return;
    }

    if (motor->direction == DIR_ANTI_CLOCKWISE) {
        motor->posInCycle -= 1;
        if (motor->posInCycle >= SM_CYCLE_SIZE) {
            motor->posInCycle = SM_CYCLE_SIZE - 1;
        }
        motor->position = (motor->position + 1) % SM_TOTAL_STEPS;
    } else {
        motor->posInCycle = (motor->posInCycle + 1) % SM_CYCLE_SIZE;
        motor->position -= 1;
        if (motor->position >= SM_TOTAL_STEPS) {
            motor->position = SM_TOTAL_STEPS - 1;
        }
    }

    stepper_output(motor);
}

// Sets the output pins of a stepper motor to reflect the current state.
void stepper_output(StepperMotor* motor) {
    for (int i = 0; i < SM_PIN_COUNT; i++) {
        PORT(motor->pinPort) &= ~(1 << motor->controlPins[i]);
    }

    PORT(motor->pinPort) |= 1 << motor->controlPins[motor->posInCycle / 2];
    if (motor->posInCycle % 2) {
        PORT(motor->pinPort) |= 1 << motor->controlPins[(motor->posInCycle / 2
            + 1) % SM_PIN_COUNT];
    }
}
